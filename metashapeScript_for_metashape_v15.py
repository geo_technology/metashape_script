# __author: GeoTech_QingQian`s Daddy
# __data: 2021-07-23

import Metashape
import os
import time

'''
    0. 预设常量
        WORK_SPACE： 工作空间，存放工程目录，数据输出位置（注意存储空间大小）；
        DATA_DIRECTORY： 数据目录，指向原始影像的父级文件夹路径。
        以上注意路径中反斜杠的转义
'''
WORK_SPACE = "E:\\workspace"
DATA_DIRECTORY = "E:\\100_0033"

def log(ctx):
        with open(os.path.join(WORK_SPACE, "log.txt"), "a") as f:
            f.write("%s     %s\n" %(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()),ctx))
        f.close()

def logEmpty():
        with open(os.path.join(WORK_SPACE, "log.txt"), "a") as f:
            f.write("\n")
        f.close()

'''1. 创建 Metashape app object'''
if os.path.exists(WORK_SPACE):
    pass
else:
    os.mkdir(WORK_SPACE)
    os.mkdir(os.path.join(WORK_SPACE, "tileZip"))

doc = Metashape.app.document

app = Metashape.Application()

projection_name = time.strftime("%Y_%m_%d_%H_%M_%S", time.localtime()) + ".psx"

doc.save(os.path.join(WORK_SPACE, projection_name))
# 初始化日志文件
log("创建 Metashape app object-----0")
logEmpty()


'''2.初始化, init chunk
        2.1 移除原始chunk
        2.2 遍历文件夹，并添加chunk
'''


# 2.1 移除原始chunk
for chunk in doc.chunks:

    doc.remove(chunk)

    log("移除原始chunk-----0")
    logEmpty()


# 2.2 遍历文件夹，并添加chunk
for root, dirs, files in os.walk(DATA_DIRECTORY):
    if root != DATA_DIRECTORY:

        chunk = doc.addChunk()  # 添加chunk

        chunk.label = root.split('\\')[-1]  # chuunk 以文件夹重命名

        chunk.crs = Metashape.CoordinateSystem("EPSG::4326")  # 定义坐标框架

        log(chunk.label + "_____chunk创建成功-----0")

        # 装配文件路径列表
        photoList = []
        for file in files:
            photoList.append(os.path.join(root, file))

        # 为 chunk添加照片
        chunk.addPhotos(photoList)

        log(chunk.label + "______chunk相片添加成功-----0")

doc.save()
logEmpty()


''' 
    3.空三处理: algin photos
        3.1 匹配照片
        3.2 对齐相机
        3.3 建立深度
'''
for chunk in doc.chunks:
    try:
        # 3.1 匹配照片
        chunk.matchPhotos(accuracy = Metashape.LowestAccuracy, generic_preselection = True, reference_preselection=False)
        # 匹配精度accuracy [HighestAccuracy, HighAccuracy, MediumAccuracy, LowAccuracy, LowestAccuracy]

        log(chunk.label + "______照片匹配成功-----0")

        #3.2 对齐相机
        chunk.alignCameras()

        log(chunk.label + "______照片对齐成功-----0")

        #3.3 建立深度
        chunk.buildDepthMaps(quality = Metashape.LowestQuality, filter = Metashape.AggressiveFiltering)
        # 建立密集点云质量quality [UltraQuality, HighQuality, MediumQuality, LowQuality, LowestQuality]

        log(chunk.label + "______照片深度建立成功-----0")

    except:
        log(chunk.label + "______空三阶段失败-----1") 

doc.save()
logEmpty()


'''
    4. 建立密集点云 dense cloud
'''
for chunk in doc.chunks:
    try:
        chunk.buildDenseCloud()

        log(chunk.label + "______密集点云创建成功-----0")
    except:
        log(chunk.label + "______密集点云创建失败-----1")
doc.save()
logEmpty()


'''
    5. 建立几何结构 build Model
'''
for chunk in doc.chunks:
    try: 
        chunk.buildModel(surface = Metashape.Arbitrary, interpolation = Metashape.EnabledInterpolation)
        
        log(chunk.label + "______几何结构创建成功-----0")
    except:
        log(chunk.label + "______几何结构创建失败-----1")  
doc.save()
logEmpty()


'''
    6. 建立正射数据 build Orthomosaic
'''
for chunk in doc.chunks:
    try:
        
        chunk.buildOrthomosaic(surface = Metashape.DataSource.ModelData)
        log(chunk.label + "______正射影像创建成功-----0")   
    except:
        log(chunk.label + "______正射影像创建失败-----1")   
doc.save()
logEmpty()


'''
    7. 导出谷歌切片
'''
for chunk in doc.chunks:
    try:
        EXPORT_FILE = os.path.join(WORK_SPACE, "tileZip",chunk.label)
        zipFile = EXPORT_FILE+".zip"
        chunk.exportOrthomosaic(zipFile, Metashape.RasterFormat.RasterFormatXYZ,Metashape.ImageFormat.ImageFormatPNG, min_zoom_level=15, max_zoom_level=23)
        
        log(chunk.label + "______谷歌切片导出成功-----0")
    except:
        log(chunk.label + "______谷歌切片导出失败-----1")

doc.save()
logEmpty()

log("全部处理完成-----0")

Metashape.app.messageBox("全部处理完成")