# MetaShape_自动化脚本

#### 说明
便捷MetaShape批处理脚本，减少人工操作负担。
日照极泰空间信息技术有限公司 -- 清浅爹 

#### 使用教程

1.  git clone git@gitee.com:geo_technology/metashape_script.git
2.  打开并编辑脚本，将工作空间WORK_SPACE、DATA_DIRECTORY指向本机的存储位置。
3.  打开Metashape,输入脚本，自动化运行。

#### 使用说明

1.  脚本适配1.7、1.5版本的Metashape，低于1.5版本的photoscan不做适配。
2.  脚本功能随时间拓展，请静静等待更新。
3.  技术交流请联系: 916988290@qq.com, 不做技术支持。

#### 脚本
1. changeImgFormate.py  png格式 XYZ标准切片转换为 webp格式；
2. metashapeScript_for_metashape_v15.py  输出xyz切片压缩包和geoTiff影像，仅支持v 1.7.0以上版本；
3. metashapeScript_for_metashape_v17.py  输出xyz切片压缩包和geoTiff影像，仅支持v 1.5.x系列版本；

